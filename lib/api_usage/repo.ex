defmodule ApiUsage.Repo do
  use Ecto.Repo,
    otp_app: :api_usage,
    adapter: Ecto.Adapters.Postgres
end
