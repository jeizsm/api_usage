FROM elixir:1.12.3-alpine

# Add build packages
RUN apk add --no-cache build-base nodejs npm

# create dir for app
RUN mkdir /app
WORKDIR /app

ENV MIX_ENV prod

# Cache elixir deps
RUN mix do local.hex --force, local.rebar --force
ADD mix.exs mix.lock ./
ADD config ./config
RUN mix do deps.get, deps.compile

# Same with npm deps
ADD assets/package.json assets/package-lock.json assets/
RUN cd assets && npm install

RUN mix esbuild.install
ADD assets ./assets
RUN mix assets.deploy

ADD . ./

RUN mix compile

# should be added on run
ENV SECRET_KEY_BASE ""
ENV DATABASE_URL ""
ENV PORT 4000

CMD ["mix", "phx.server"]
