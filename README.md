# ApiUsage

    There are only two hard things in Computer Science: cache invalidation and naming things.

    -- Phil Karlton


# Goals

1. It's a proof a concept so it shouldn't take too much time
2. Simplicity, it's should be as simple as possible, not overcomplicated
3. A little as code as possible

## Approach

So because of three main goals, I think my solution will be using one phoenix application
with PostgreSQL as database. Phoenix has almost everything that needed Phoenix LiveView
and Phoenix PubSub(via pg2) for implementing real time updates of users data table.
For adding styles and interactivity to the table I would use TailwindCSS and AlpineJS.
We would have one subscription for updates of every user, and we would decide in LiveView
should we sent an update to page.

For authentication I would use [phx.gen.auth](https://hexdocs.pm/phoenix/mix_phx_gen_auth.html)
with [argon2](https://hexdocs.pm/argon2_elixir/api-reference.html) because argon2 is considered
most secure option right now.

API would be in the same application as web. It would be authenticated via same session token
as used to sign in in web app for simplicity. It would be sent via authorisation header.
For rate limiting I would use [hammer_plug](https://hexdocs.pm/hammer_plug/readme.html).
It looks a little abandoned, but I hope it works well. But another option worth considering
is [ex_rated](https://github.com/grempe/ex_rated) and wrote a little plug by myself.

## Trade-offs

Main trade-off is that we have only one application, for example if company grows
and we want to separate web app and api to split work between different teams it
would be harder to do it then you already have written everything in one application,
if code isn't decoupled enough. If I would have some time left in the end I show
approach with three different applications core, web and api.

Another small and easily fixable trade-off if we would want to have access to PubSub
from service written in different language, we would need another adapter
because pg2 using Distributed Elixir, directly exchanging notifications between servers.

Another tradeoff related to PubSub system it's that we have one subscription for everyone.
Whe number of user increases and we would need to deliver more updates, it would be worth
splitting one channel for all to different channels. Simpliest solution will be just
sending updates of different users to different channels. Or we can create some kind
of system when client can subscribe to get updates of users that pass some filters.

# Setup and running

To start your Phoenix server:

  * Install docker and docker-compose
  * Run `docker-compose up` to run application

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.
