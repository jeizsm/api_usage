defmodule ApiUsageWeb.PageController do
  use ApiUsageWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
